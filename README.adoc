= Multi Weather

Weather forecast from several providers

== Info for skinners

All values returned by the addon will include their units.
Skinners won't have to bother with it.

== Default Kodi weather labels

.Current
[cols="1,1,1"]
|===
| Label | Description | Example value

| Current.Location | |
| Current.Condition | |
| Current.Temperature | |
| Current.Wind | |
| Current.WindDirection | |
| Current.Humidity | |
| Current.FeelsLike | |
| Current.DewPoint | |
| Current.UVIndex | |
| Current.ConditionIcon | | 28.png
| Current.FanartCode | |
|===


.Day [0-6]
[cols="1,1,1"]
|===
| Label | Description | Example value

| Day%i.Title | |
| Day%i.HighTemp | |
| Day%i.LowTemp | |
| Day%i.Outlook | |
| Day%i.OutlookIcon | |
| Day%i.FanartCode | |
|===


.WeatherProvider
[cols="1,1,1"]
|===
| Label | Description | Example value

| WeatherProvider | |
| WeatherProviderLogo | |
|===

== Extended weather labels

.Forecast
[cols="1,1,1"]
|===
| Label | Description | Example value

| Forecast.IsFetched | |
| Forecast.City | |
| Forecast.Country | |
| Forecast.Latitude | |
| Forecast.Longitude | |
| Forecast.Updated | Date and time the forecast was retrieved by Yahoo |
|===


.Current
[cols="1,1,1"]
|===
| Label | Description | Example value

| Current.IsFetched | |
| Current.Visibility | Visible distance |
| Current.Pressure | Air pressure |
| Current.SeaLevel | Pressure at sealevel |
| Current.Precipitation | Probability |
| Current.Cloudiness | Cloud coverage |
|===

.Today
[cols="1,1,1"]
|===
| Label | Description | Example value

| Today.IsFetched | |
| Today.Sunrise | |
| Today.Sunset | |
| Today.MoonPhase | |
|===


.Hourly [1-24]
[cols="1,1,1"]
|===
| Label | Description | Example value

| Hourly.IsFetched | |
| Hourly.%i.Time | | 12:00
| Hourly.%i.LongDate | |
| Hourly.%i.ShortDate | |
| Hourly.%i.Outlook | | Very heavy rain
| Hourly.%i.OutlookIcon | |
| Hourly.%i.FanartCode | |
| Hourly.%i.Temperature | |
| Hourly.%i.FeelsLike | |
| Hourly.%i.Humidity | |
| Hourly.%i.Precipitation | Probability of precipitation |
| Hourly.%i.WindSpeed | |
| Hourly.%i.WindDirection | | SSW
| Hourly.%i.WindDegree | | 220°
| Hourly.%i.DewPoint |
|===



.Daily [1-10] (Yahoo)
[cols="1,1,1"]
|===
| Label | Description | Example value

| Daily.IsFetched | |
| Daily.%i.LongDay | | Monday
| Daily.%i.ShortDay | | Mon
| Daily.%i.LongDate | | 1 January
| Daily.%i.ShortDate | | 1 Jan
| Daily.%i.Outlook | | Mostly Cloudy
| Daily.%i.OutlookIcon | |
| Daily.%i.FanartCode | |
| Daily.%i.Humidity | |
| Daily.%i.DewPoint | |
| Daily.%i.Precipitation | Probability of precipitation |
| Daily.%i.HighTemperature | Highest temperature that will be reached today |
| Daily.%i.LowTemperature | Lowest temperature that will be reached today |
|===


.Daily [1-16] (Weatherbit)
[cols="1,1,1"]
|===
| Label | Description | Example value

| Daily.IsFetched | |
| Daily.%i.LongDay | | Monday
| Daily.%i.ShortDay | | Mon
| Daily.%i.LongDate | | 1 January
| Daily.%i.ShortDate | | 1 Jan
| Daily.%i.Outlook | | Very heavy rain
| Daily.%i.OutlookIcon | |
| Daily.%i.FanartCode | |
| Daily.%i.WindSpeed | |
| Daily.%i.WindDirection | | SSW
| Daily.%i.WindDegree | | 220°
| Daily.%i.Humidity | |
| Daily.%i.DewPoint | |
| Daily.%i.WindGust | |
| Daily.%i.HighTemperature | Highest temperature that will be reached today |
| Daily.%i.LowTemperature | Lowest temperature that will be reached today |
| Daily.%i.Temperature | Average temperature |
| Daily.%i.HighFeelsLike | |
| Daily.%i.LowFeelsLike | |
| Daily.%i.FeelsLike | Same as HighFeelsLike |
| Daily.%i.Pressure | |
| Daily.%i.SeaLevel | |
| Daily.%i.Cloudiness | |
| Daily.%i.CloudsLow | Cloud coverage at 0-3km hight |
| Daily.%i.CloudsMid | Cloud coverage at 3-5km hight |
| Daily.%i.CloudsHigh | Cloud coverage at >5km hight |
| Daily.%i.Snow | Amount of snow |
| Daily.%i.SnowDepth | Depth of snow on the ground |
| Daily.%i.Precipitation | Total amount of rain and snow |
| Daily.%i.Probability | Probability of precipitation |
| Daily.%i.UVIndex | |
| Daily.%i.Visibility | |
| Daily.%i.Sunrise | |
| Daily.%i.Sunset | |
| Daily.%i.Moonrise | |
| Daily.%i.Moonset | |
| Daily.%i.MoonPhase | Moon phase fraction |
| Daily.%i.Ozone | Average ozone level |
|===


.MAP [1-5] (Openweathermap)
[cols="1,1,1"]
|===
| Label | Description | Example value

| Map.IsFetched | |
| Map.%i.Area | |
| Map.%i.Layer | |
| Map.%i.Heading | |
| Map.%i.Legend | |
